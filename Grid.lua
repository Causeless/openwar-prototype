class = require "hump.class"
vec2 = require "hump.vector"

Grid = class{}

--[[function Grid:init(positions)
	self.cellSize = 12
	self.cells = {}
	for i = -16, 16 do
		self.cells[i] = {}
		for j = -16, 16 do
			self.cells[i][j] = {}	-- Create blank grid
		end
	end
	
	self.positions = positions
end

function Grid:rebuild(arrayLen)
	self:clear()
	
	for i = 1, arrayLen do
		local x = math.floor(self.positions[i].x / self.cellSize)
		local y = math.floor(self.positions[i].y / self.cellSize)
		
		if not self.cells[x] then self.cells[x] = {} end
		if not self.cells[x][y] then self.cells[x][y] = {} end
		table.insert(self.cells[x][y], i)
	end
end

function Grid:getTableForCells(point, radius)
	local linearTable = {}
	
	-- We check every cell covered by the AABB of the circle
	
	local minCellX = math.floor((point.x - radius) / self.cellSize)
	local minCellY = math.floor((point.y - radius) / self.cellSize)
	local maxCellX = math.floor((point.x + radius) / self.cellSize)
	local maxCellY = math.floor((point.y + radius) / self.cellSize)
	
	for x = minCellX, maxCellX do
		if not self.cells[x] then self.cells[x] = {} end
		for y = minCellY, maxCellY do
			if not self.cells[x][y] then self.cells[x][y] = {} end
			for i, j in pairs(self.cells[x][y]) do
				table.insert(linearTable, j)
			end
		end
	end
	
	return linearTable
end]]

function Grid:init(positions)
	self.cellSize = 6
	self.cells = {}
	
	self.positions = positions
end

function Grid:rebuild(arrayLen)
	self:clear()
	
	for i = 1, arrayLen do
		local x = math.floor(self.positions[i].x / self.cellSize)
		local y = math.floor(self.positions[i].y / self.cellSize)
		
		local hash = self:hash(x, y)
		
		if not self.cells[hash] then self.cells[hash] = {} end
		table.insert(self.cells[hash], i)
	end
end

function Grid:getTableForCells(point, radius)
	local linearTable = {}
	
	-- We check every cell covered by the AABB of the circle
	
	local minCellX = math.floor((point.x - radius) / self.cellSize)
	local minCellY = math.floor((point.y - radius) / self.cellSize)
	local maxCellX = math.floor((point.x + radius) / self.cellSize)
	local maxCellY = math.floor((point.y + radius) / self.cellSize)
	
	for x = minCellX, maxCellX do
		for y = minCellY, maxCellY do
			local hash = self:hash(x, y)
			if self.cells[hash] then
				for i, j in pairs(self.cells[hash]) do
					table.insert(linearTable, j)
				end
			end
		end
	end
	
	return linearTable
end

function Grid:hash(x, y)
	local h1 = 0x8da6b343
	local h2 = 0xd8163841
	
	local n = math.floor(h1 * x + h2 * y)
	return n
end

function Grid:clear()
	self.cells = {}
end