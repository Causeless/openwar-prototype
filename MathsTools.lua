function math.round(x, deci)
  deci = 10^(deci or 0)
  return math.floor(x*deci+.5)/deci
end

function floatEquals(a, b, epsilon) -- Need to have this huge function because of the complexities of comparing floats.
	local absA = math.abs(a)
	local absB = math.abs(b)
	local diff = math.abs(a - b)
	
	local minNormal = 2.225074e-308
	local maxValue = 1.7976931348623157e+308 -- Max non-infinite value a float can hold
	
	if (a == b) then
		return true; -- avoids division by 0
	elseif (a == 0 or b == 0 or diff < minNormal) then 
		-- a or b are zero or both are extremely close to it
		-- relative error is less meaningful here
		return diff < (epsilon * minNormal)
	else -- use relative error
		return diff / math.min((absA + absB), maxValue) < epsilon
	end
end