require "MathsTools"
require "VectorTools"
class = require "hump.class"
vec2 = require "hump.vector"

FormationManager = class{}

function FormationManager:init(host)
	self.host = host
	
	self.targets = {}
	self.rowCount = 0
	self.colCount = 0
	
	local averagePos
	if self.host.soldierCount > 0 then
		averagePos = self.host.soldierPos[1]
		for i = 2, self.host.soldierCount do
			averagePos = averagePos + self.host.soldierPos[i]
		end
		averagePos = averagePos / self.host.soldierCount
	else
		averagePos = vec2(0, 0)
	end
	
	self.pos = averagePos
	self.targetPos = vec2(0, 0)
	self.vel = vec2(0, 0)
end

-- Todo: handle dead soldiers when implemented, check for zero length lines
function FormationManager:setFormation(formation)
	self.targets, self.targetPos = self:convertWorldTargetsToUnit(formation.targets)
	
	self.rowCount = formation.rowCount
	self.colCount = formation.colCount
end

function FormationManager:update(dt)
	local averagePos
	local averageVel
	if self.host.soldierCount > 0 then
		averagePos = self.host.soldierPos[1]
		averageVel = self.host.soldierVel[1]
		for i = 2, self.host.soldierCount do
			averagePos = averagePos + self.host.soldierPos[i]
			averageVel = averageVel + self.host.soldierVel[i]
		end
		averagePos = averagePos / self.host.soldierCount
		averageVel = averageVel / self.host.soldierCount
	else
		averagePos = vec2(0, 0)
		averageVel = vec2(0, 0)
	end
	
	self.pos = averagePos
	--self.vel = averageVel
	
	local vecToTarget = self.targetPos - self.pos
	local steering = vecToTarget -- Target is path from formation leader to target
	--steering = truncate(steering, self.host.maxAccel)
	
	self.vel = self.vel - (self.vel * 2 * dt)	-- Friction	
    self.vel = self.vel + (steering * dt)
	self.vel = truncate(self.vel, self.host.walkSpeed)
		
    self.pos = self.pos + (self.vel * dt)
end

function FormationManager:getTargetsWorldSpace()
	local targets = {}
	
	for i = 1, #self.targets do
		targets[i] = {}
		targets[i].pos = self.targets[i].pos + self.pos + self.vel*2.0 -- TODO: Calculate the multiplier via figuring out the intended walking speed
		targets[i].dir = self.targets[i].dir
		targets[i].rank = self.targets[i].rank
	end
	
	return targets
end

function FormationManager:getTargetsUnitSpace()
	return self.targets
end

function FormationManager:getTargetsFinalLocation()
	local targets = {}

	for i = 1, #self.targets do
		targets[i] = {}
		targets[i].pos = self.targets[i].pos + self.targetPos
		targets[i].dir = self.targets[i].dir
		targets[i].rank = self.targets[i].rank
	end
	
	return targets
end

function FormationManager:convertWorldTargetsToUnit(targets)
	local newTargets = {}
	local targetPos = vec2(0, 0)
	
	for i = 1, #targets do
		targetPos = targetPos + targets[i].pos
	end
	targetPos = targetPos / #targets
	
	for i = 1, #targets do
		newTargets[i] = {}
		newTargets[i].pos = targets[i].pos - targetPos
		newTargets[i].dir = targets[i].dir
		newTargets[i].rank = targets[i].rank
	end
	
	return newTargets, targetPos
end

function FormationManager:getExtent()
	return math.max(self.rowCount, self.colCount)
end