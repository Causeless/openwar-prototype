-- OpenWar

require "Unit"
require "SteeringManager"
require "FormationCreator"
require "RenderingManager"
require "VectorTools"
require "MathsTools"
vec2 = require "hump.vector"
Camera = require "hump.camera"
debugGraph = require 'thirdparty.debugGraph'
ProFi = require "thirdparty.ProFi"

-- Use this function to perform your initial setup
function love.load()
	yScale = 0.75 -- We yScale to create an isometric game world

	love.graphics.setBackgroundColor(40, 40, 50)
	
	font = love.graphics.newImageFont("resources/font.png",
    " abcdefghijklmnopqrstuvwxyz" ..
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..
    "123456789.,!?-+/():;%&`'*#=[]\"")
	love.graphics.setFont(font)
	
	Unit.getTargetCanvas() -- Force initialization of the target canvas before the cameras moves etc
	
	local unit1_soldierCount = 240
	local unit2_soldierCount = 240
	
    testUnit1 = Unit(unit1_soldierCount, "resources/bSpearman/", 0, 0, 255)
	testUnit2 = Unit(unit2_soldierCount, "resources/bSpearman/", 255, 0, 0)
	
	local columns = 40
	local unit1_width = testUnit1.soldierRadius*2 * columns
	local unit2_width = testUnit2.soldierRadius*2 * columns
	
	local unitDistanceAway = 300
	
	local unit1_start = vec2(-unitDistanceAway*0.5, -unit1_width*0.5)
	local unit1_end   = vec2(-unitDistanceAway*0.5,  unit1_width*0.5)
	
	local unit2_start = vec2( unitDistanceAway*0.5, -unit2_width*0.5)
	local unit2_end   = vec2( unitDistanceAway*0.5,  unit2_width*0.5)
	
	formationCreator = FormationCreator()
	local formation1 = formationCreator:gridFormationForLine(unit1_start, unit1_end, unit1_soldierCount)
	local formation2 = formationCreator:gridFormationForLine(unit2_end, unit2_start, unit2_soldierCount) -- Reverse start and end so formation faces opposite way
	
	testUnit1:setPositions(formation1.targets)
	testUnit2:setPositions(formation2.targets)
	
	testUnit1.formation:setFormation(formation1)
	testUnit2.formation:setFormation(formation2)
	
	local soldierTargetPairs = SlotAssignmentStrategy:assignTargetsID(testUnit1, formation1.targets)
	testUnit1:setSoldierTargetPairs(soldierTargetPairs)
			
	local soldierTargetPairs2 = SlotAssignmentStrategy:assignTargetsID(testUnit2, formation2.targets)
	testUnit2:setSoldierTargetPairs(soldierTargetPairs2)
	
	testUnit1.steering:addCheckedUnit(testUnit2)
	testUnit2.steering:addCheckedUnit(testUnit1)

	showTargets = false
	
	startMousePos = vec2(0, 0)
	currentDragPos = vec2(0, 0)
	
	frameCount = 0

	renderTime = 0
	
	debug_draw = false
	
	accumulator = 0
	currentTime = 0
	
	renderFPSGoal = 60
	renderTimeLimit = (1/renderFPSGoal)*2
	
	simFPS = 15
	step = 1/simFPS
	gameSimTime = 0
	
	timeModifier = 1.0
	
	devCam = Camera()
	devCam:lookAt(0, 0)
	
	local unitList = {testUnit1, testUnit2}
	
	renderingManager = RenderingManager(unitList)
	
	local screenX, screenY = love.graphics.getDimensions()
	
	fpsGraph = debugGraph:new('fps', screenX - 90, 10)
	memGraph = debugGraph:new('mem', screenX - 90, 60)
	--dtGraph = debugGraph:new('dt',  screenX - 90, 110)
	
	--ProFi:start()
end

function getWorldMousePos()
	local x, y = devCam:mousepos()
	
	return x, y * 1/yScale
end

function love.keypressed(key)
	if key == 'escape' then
		love.event.quit()
	end
end

function love.wheelmoved(x, y)
	if y == 1.0 then
		devCam:zoom(1.1)
	elseif y == -1.0 then
		devCam:zoom(0.9)
	end
end

function love.mousereleased(x, y, button)
	if button == 3 then
		camDragging = false
	elseif button == 1 then
		if mouseCurrentDragPos then
			local unitPrevExtent = testUnit1.formation:getExtent()
		
			local formation = formationCreator:gridFormationForLine(mouseBeginDragPos, mouseCurrentDragPos, testUnit1.soldierCount)
			testUnit1.formation:setFormation(formation)
			
			local unitCurrExtent = testUnit1.formation:getExtent()
			
			local targets = testUnit1.formation:getTargetsWorldSpace()
			--local targets = testUnit1.formation:getTargetsFinalLocation()
			
			local soldierTargetPairs = nil
			-- If the formation has extended then fill from the outside, else the inside
			if unitCurrExtent > unitPrevExtent then
				soldierTargetPairs = SlotAssignmentStrategy:assignTargetsEdgeIn(testUnit1, targets)
			else
				soldierTargetPairs = SlotAssignmentStrategy:assignTargetsCentreOut(testUnit1, targets)
			end
			
			--soldierTargetPairs = SlotAssignmentStrategy:assignTargetsStableMarriage(testUnit1, targets)
			
			testUnit1:setSoldierTargetPairs(soldierTargetPairs)
		end
		mouseDraggingL = false
		showLUnitLayout = false
		--mouseBeginDragPos, mouseCurrentDragPos = nil
	elseif button == 2 then
		if mouseCurrentDragPos then
			local unitPrevExtent = testUnit2.formation:getExtent()
		
			local formation = formationCreator:gridFormationForLine(mouseBeginDragPos, mouseCurrentDragPos, testUnit1.soldierCount)
			testUnit2.formation:setFormation(formation)
			
			local unitCurrExtent = testUnit2.formation:getExtent()
			
			local targets = testUnit2.formation:getTargetsWorldSpace()
			--local targets = testUnit2.formation:getTargetsFinalLocation()
			
			local soldierTargetPairs = nil
			-- If the formation has extended then fill from the outside, else the inside
			if unitCurrExtent > unitPrevExtent then
				soldierTargetPairs = SlotAssignmentStrategy:assignTargetsEdgeIn(testUnit2, targets)
			else
				soldierTargetPairs = SlotAssignmentStrategy:assignTargetsCentreOut(testUnit2, targets)
			end
			
			testUnit2:setSoldierTargetPairs(soldierTargetPairs)
		end
		mouseDraggingR = false
		showRUnitLayout = false
		--mouseBeginDragPos, mouseCurrentDragPos = vec2(0, 0)
	end
end

function love.update(dt)
	renderTime = renderTime + dt * timeModifier
	
	if love.mouse.isDown(1) then
		if mouseDraggingL then
			local x, y = getWorldMousePos()
			mouseCurrentDragPos = vec2(x, y)
			
			if mouseCurrentDragPos then
				targets1 = formationCreator:gridFormationForLine(mouseBeginDragPos, mouseCurrentDragPos, testUnit1.soldierCount).targets
				showLUnitLayout = true
			end
		else
			mouseDraggingL = true
			local x, y = getWorldMousePos()
			mouseBeginDragPos = vec2(x, y)
		end
	
	elseif love.mouse.isDown(2) then
		if mouseDraggingR then
			local x, y = getWorldMousePos()
			mouseCurrentDragPos = vec2(x, y)
			
			if mouseCurrentDragPos then
				targets2 = formationCreator:gridFormationForLine(mouseBeginDragPos, mouseCurrentDragPos, testUnit2.soldierCount).targets
				showRUnitLayout = true
			end
		else
			mouseDraggingR = true
			local x, y = getWorldMousePos()
			mouseBeginDragPos = vec2(x, y)
		end
		--[[for i = 1, testUnit1.soldierCount do
			local x, y = devCam:mousepos()
			mousePos = vec2(x, y)
			if (mousePos - testUnit1.soldierPos[i]):len2() < testUnit1.soldierRadius * testUnit1.soldierRadius then
				testUnit1:removeSoldier(i)
				if mouseCurrentDragPos then testUnit1:setTargets(testFormationManager:gridFormationForLine(mouseBeginDragPos, mouseCurrentDragPos, testUnit1.soldierCount)) end
				break
			end
		end]]
    end

	if love.mouse.isDown(3) then
		if camDragging == true then
			--local x, y = getWorldMousePos()
			local x, y = devCam:mousepos()
			camCurrentDragPos = vec2(x, y)
			local moveVec = camStartDragPos - camCurrentDragPos
			devCam:move(moveVec.x, moveVec.y)
		else
			camDragging = true
			--local x, y = getWorldMousePos()
			local x, y = devCam:mousepos()
			camStartDragPos = vec2(x, y)
		end
	end
	
	if love.keyboard.isDown("space") then
		showTargets = true
	else
		showTargets = false
	end
	
	if love.keyboard.isDown("backspace") then
		testUnit1:removeTargets()
		testUnit2:removeTargets()
	end
	
	-- Add dt * timeModifier on, to support fast/slow motion
	accumulator = accumulator + (dt * timeModifier)
	
	-- Should update animations outside of the simulation stepper, as it doesn't affect the simulation and looks smoother at low step rates
	-- Need to figure out how to make this work in the situation that the sim stepper slows down/stops for whatever reason...
	-- may need to do it the same way the rendering manager does via interpolation
	-- And figure out anim frame by distance travelled instead of direct velocity?
	
	while accumulator >= step do
		if dt > renderTimeLimit then -- We're behind schedule!
			print("Accumulator is behind - skipping a frame")
			--simFPS = (simFPS * 0.966) + (0.034 / step)
			accumulator = accumulator - step -- Skip a frame, we run in slow-motion instead of causing render FPS to plummet
		else
			renderingManager:updatePreSim(gameSimTime)
				
			simFPS = (simFPS * 0.966) + (0.034 / step)
			gameSimTime = gameSimTime + step
				
			renderingManager:updatePostSim(gameSimTime)
			
			testUnit1:update(step)
			testUnit2:update(step)
				
			testUnit1.animator:updateAnimStates(step)
			testUnit2.animator:updateAnimStates(step)
				
			accumulator = accumulator - step
		end
	end
	
	renderingManager:updateRender(renderTime)
	
	frameCount = frameCount + 1
	
	-- Update the graphs
	fpsGraph:update(dt)
	memGraph:update(dt)

	--[[-- Update our custom graph
	dtGraph:update(dt, math.floor(dt * 1000))
	dtGraph.label = 'DT: ' ..  math.round(dt, 4)]]
end

function love.draw()
	devCam:attach()
	
	love.graphics.push()
	love.graphics.scale(1, yScale)
	
    if showTargets then
		testUnit1:drawTargets()
		testUnit2:drawTargets()
	end
	
	if showLUnitLayout == true then
		if targets1 then
			for i = 1, testUnit1.soldierCount do
				Unit:drawTarget(targets1[i].pos, targets1[i].dir)
			end
		end
	end
	
	if showRUnitLayout == true then
		if targets2 then
			for i = 1, testUnit2.soldierCount do
				Unit:drawTarget(targets2[i].pos, targets2[i].dir)
			end
		end
	end
	
    --testUnit1:drawVelocity()
	--testUnit2:drawVelocity()
	
    --testUnit1:drawSteering()
	--testUnit2:drawSteering()
	
	love.graphics.pop() -- We do not scale the UI (or soldiers, as they are drawn isometrically already)
	
	renderingManager:depthSort()
	renderingManager:draw(yScale, debug_draw)
	
	devCam:detach()
	
	love.graphics.print("Simulation Speed:", 10, 10)
	
	love.graphics.print(math.round(simFPS), 180, 10)
	
	love.graphics.print("OpenWar Prototype", 300, 10)
	
	local r, g, b = love.graphics.getColor()
	
	local renderFPS = fpsGraph.data[#fpsGraph.data]
	
	if renderFPS >= 58.0 then love.graphics.setColor(0, 255, 0) 
	elseif renderFPS >= 28.0 then love.graphics.setColor(0, 0, 255) 
	elseif renderFPS < 28.0 then love.graphics.setColor(255, 0, 0) 
	end
	
	fpsGraph:draw()
	
	love.graphics.setColor(r, g, b)
	
	memGraph:draw()
	--dtGraph:draw()
end

function love.quit()
	--ProFi:stop()
	--ProFi:writeReport("profiling.txt")
end