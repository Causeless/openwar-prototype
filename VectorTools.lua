function truncate(vec, max)
    if vec:len() > max then
        return vec:normalized() * max
    else
        return vec
    end
end

function setLength(vec, len)
    return vec:normalized() * len
end

function setLengthInplace(vec, len)
    vec:normalize_inplace()
	vec.x = vec.x * len
	vec.y = vec.y * len
end

function scaleVector(vec, newMax)
	oldMax = vec:len()
	return setLength(vec, (vec:len() * newMax) / oldMax)
end

function reverseVector(vec, minLen, maxLen)	-- "Reverses" a vector's length - if vec:len() = 25 and minLen = 0, maxLen = 100, returned vector's length would be 75
	return setLength(vec, (maxLen + minLen) - vec:len())
end

function lineIntersectsCircle(lineStart, lineEnd, circlePos, circleRad)
	local circleRadSqr = circleRad * circleRad	-- Squared distance for performance
	local line = lineEnd - lineStart
	local circlePos2 = circlePos - lineStart
	
	return circlePos2:dist2(line) <= circleRadSqr or circlePos2:dist2(line * 0.5) <= circleRadSqr --or circlePos2:dist2(lineStart) <= circleRadSqr	-- Last or seems to have bug? isn't needed anyways
end

function lineIntersectsCircle2(lineStart, lineEnd, circlePos, circleRadSqr)
	local line = lineEnd - lineStart
	local circlePos2 = circlePos - lineStart
	
	return circlePos2:dist2(line) <= circleRadSqr or circlePos2:dist2(line * 0.5) <= circleRadSqr --or circlePos2:dist2(lineStart) <= circleRadSqr	-- Last or seems to have bug? isn't needed anyways
end

function pointInCircle(point, circlePos, circleRad)
	return point:dist2(circlePos) < circleRad * circleRad
end

function pointInCircle2(point, circlePos, circleRadSqr)
	return point:dist2(circlePos) < circleRadSqr
end

function minDistanceToLine(v, w, p)
	local l2 = v:dist2(w)
	local t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2
	local projection = v + t * (w - v)
	return p:dist2(projection)
end

function minDistanceToLineSegment(v, w, p)
	local l2 = v:dist2(w)
	local t = ((p.x - v.x) * (w.x - v.x) + (p.y - v.y) * (w.y - v.y)) / l2
	
	if t < 0.0 then 
		return (p - v):len()
	elseif t > 1.0 then
		return (p - w):len() 
	end
	
	local projection = v + t * (w - v)
	return p:dist2(projection)
end

function lerp(vec1, vec2, t)
	local dist = vec2-vec1
	return vec1 + dist * t
end