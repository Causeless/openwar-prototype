require "MathsTools"
class = require "hump.class"
vec2 = require "hump.vector"

FormationCreator = class{}

function FormationCreator:init(host)
	self.horizontalSpacingRadius = 11	-- Spacing between individual soldiers
	self.verticalSpacingRadius = 11
end

-- Todo: handle dead soldiers when implemented, check for zero length lines

function FormationCreator:gridFormationForLine(lineStart, lineEnd, soldierCount)	-- For Total-War like click+drag formations.
	local lineDirAlong = (lineEnd - lineStart):normalize_inplace()
	local lineDirBack = lineDirAlong:perpendicular()
	
	local forwards = lineDirBack * -1
	
	local formationLength = (lineEnd - lineStart):len()
	local soldiersPerRow = (math.round(formationLength / self.horizontalSpacingRadius))
	local soldiersLeft = soldierCount - 1
	
	local targets = {}
	
	local row = 0
	local soldierPos = vec2(0, 0)
	
	while soldiersLeft >= soldiersPerRow do	-- while there are enough soldiers left for another complete row
		for column = 0, soldiersPerRow do
			soldiersLeft = soldiersLeft - 1
			soldierPos = lineStart + (lineDirAlong * self.horizontalSpacingRadius * column)	-- Add horizontal spacing
			soldierPos = soldierPos + (lineDirBack * self.verticalSpacingRadius * row)	-- Add vertical spacing
			table.insert(targets, { pos = soldierPos, dir = math.atan2(forwards.y, forwards.x), rank = vec2(column, row)})
		end
		row = row + 1
	end
	
	local backRowSlideAmount = ((soldiersPerRow - soldiersLeft) / 2)
	backRowSlideAmount = lineDirAlong * (backRowSlideAmount * self.horizontalSpacingRadius)
	
	for backRowColumn = 0, soldiersLeft do	-- Need a partial row
		soldiersLeft = soldiersLeft - 1
		local soldierPos = lineStart + ((lineDirAlong * self.horizontalSpacingRadius * backRowColumn) + backRowSlideAmount)
		soldierPos = soldierPos + (lineDirBack * self.verticalSpacingRadius * row)
		table.insert(targets, { pos = soldierPos, dir = math.atan2(forwards.y, forwards.x), rank = vec2(column, row)})
	end
	
	return {targets = targets, colCount = soldiersPerRow + 1, rowCount = row}
end

function FormationCreator:hexFormationForLine(lineStart, lineEnd, soldierCount)
	local lineDirAlong = (lineEnd - lineStart):normalize_inplace()
	local lineDirBack = lineDirAlong:perpendicular()
	
	local forwards = lineDirBack * -1
	
	local formationLength = (lineEnd - lineStart):len()
	local soldiersPerRow = (round(formationLength / self.horizontalSpacingRadius))
	local soldiersLeft = soldierCount - 1
	
	local targets = {}
	
	local row = 0
	local soldierPos = vec2(0, 0)
	
	while soldiersLeft >= soldiersPerRow do	-- while there are enough soldiers left for another complete row
		if row % 2 == 0 then
			for column = 0, soldiersPerRow do
				soldiersLeft = soldiersLeft - 1
				soldierPos = lineStart + (lineDirAlong * self.horizontalSpacingRadius * column)	-- Add horizontal spacing
				soldierPos = soldierPos + (lineDirBack * self.verticalSpacingRadius * row)	-- Add vertical spacing
				table.insert(targets, { pos = soldierPos, dir = math.atan2(forwards.y, forwards.x), rank = vec2(column, row)})
			end
		else
			for column = 0, soldiersPerRow - 1 do
				soldiersLeft = soldiersLeft - 1
				soldierPos = lineStart + (lineDirAlong * self.horizontalSpacingRadius * column) + (lineDirAlong * self.horizontalSpacingRadius * 0.5)	-- Add horizontal spacing
				soldierPos = soldierPos + (lineDirBack * self.verticalSpacingRadius * row)	-- Add vertical spacing
				table.insert(targets, { pos = soldierPos, dir = math.atan2(forwards.y, forwards.x), rank = vec2(column, row)})
			end
		end
		row = row + 1
	end
	
	local backRowSlideAmount = ((soldiersPerRow - soldiersLeft) / 2)
	backRowSlideAmount = lineDirAlong * (backRowSlideAmount * self.horizontalSpacingRadius)
	
	for backRowColumn = 0, soldiersLeft do	-- Need a partial row
		soldiersLeft = soldiersLeft - 1
		local soldierPos = lineStart + ((lineDirAlong * self.horizontalSpacingRadius * backRowColumn) + backRowSlideAmount)
		soldierPos = soldierPos + (lineDirBack * self.verticalSpacingRadius * row)
		table.insert(targets, { pos = soldierPos, dir = math.atan2(forwards.y, forwards.x), rank = vec2(column, row)})
	end
	
	return {targets = targets, colCount = soldiersPerRow + 1, rowCount = row}
end
