class = require "hump.class"
vec2 = require "hump.vector"
require "VectorTools"

RenderingManager = class{}

function orderY(y1, y2)
	return y1[3] < y2[3]
end

function RenderingManager:init(unitList)
	self.unitList = unitList
	self.renderList = {}
	
	for i = 1, #unitList do
		for j = 1, unitList[i].soldierCount do
			table.insert(self.renderList, {i, j, unitList[i].soldierPos[j].y})
		end
	end
	
	table.sort(self.renderList, orderY)
	
	self.lastFramePositions = {} -- For interpolation
	for i = 1, #self.unitList do
		self.lastFramePositions[i] = self.unitList[i]
		for j = 1, self.unitList[i].soldierCount do
			self.lastFramePositions[i][j] = self.unitList[i].soldierPos[j]
		end
	end
	
	self.lastSimTime = 0
	self.currentSimTime = 1/0 -- We set it to some arbitrary number above 0 initially to avoid an infinite loop as renderTime can fit "under" it
	self.renderTime = 0
end

function RenderingManager:updatePreSim(simTime)
	self.lastSimTime = simTime
	
	self.lastFramePositions = {}
	for i = 1, #self.unitList do
		self.lastFramePositions[i] = self.unitList[i]
		for j = 1, self.unitList[i].soldierCount do
			self.lastFramePositions[i][j] = self.unitList[i].soldierPos[j]
		end
	end
end

function RenderingManager:updatePostSim(simTime)
	self.currentSimTime = simTime
end

function RenderingManager:updateRender(renderTime)
	self.renderTime = renderTime
end

function RenderingManager:depthSort()
	self.renderList = {}
	
	for i = 1, #self.unitList do
		for j = 1, self.unitList[i].soldierCount do
			table.insert(self.renderList, {i, j, self.unitList[i].soldierPos[j].y})
		end
	end
	
	table.sort(self.renderList, orderY)
end

function RenderingManager:draw(yScale, debug_draw)
	if debug_draw then
		love.graphics.push()
		love.graphics.scale(1, yScale)
		love.graphics.setLineWidth(0.25)
		for key, unit in pairs(self.unitList) do
			for i = 1, #unit.steering.checks do
				love.graphics.line(unit.steering.checks[i][1].x, unit.steering.checks[i][1].y, unit.steering.checks[i][2].x, unit.steering.checks[i][2].y)
			end
		end
		love.graphics.pop()
	
		for i = 1, #self.renderList do
			love.graphics.push()
			love.graphics.scale(1, yScale)
			
			local id = self.renderList[i][2]
			local unit = self.unitList[ self.renderList[i][1] ]
			
			local lastFramePos = self.lastFramePositions[self.renderList[i][1]][id]
			
			local stepSize = self.currentSimTime - self.lastSimTime
			while self.renderTime - stepSize > self.currentSimTime do
				self.renderTime = self.renderTime - stepSize
			end
			
			local interpTime = (self.renderTime - stepSize - self.lastSimTime) / stepSize -- 0 when matching past frame frame, 1 when matching current frame
			local interpolatedPos = lerp(lastFramePos, unit.soldierPos[id], interpTime)
		
			local dir = unit.soldierDir[id]
			local radius = 4
			local halfpi = math.pi * 0.5
	
			local invDir = dir - math.pi
	
			local left = invDir - (halfpi*0.5)
			local right = invDir + (halfpi*0.5)
	
			local dirVec = vec2(math.cos(dir), math.sin(dir)) * radius
			local leftVec = vec2(math.cos(left), math.sin(left)) * radius
			local rightVec = vec2(math.cos(right), math.sin(right)) * radius
			local pos = interpolatedPos
			love.graphics.polygon("fill", pos.x+leftVec.x, pos.y+leftVec.y, pos.x+dirVec.x, pos.y+dirVec.y, pos.x+rightVec.x, pos.y+rightVec.y)
			
			--[[local dirVector = vec2(math.cos(unit.soldierDir[id]), math.sin(unit.soldierDir[id])) * unit.soldierRadius
		
			love.graphics.circle("fill", interpolatedPos.x, interpolatedPos.y, unit.soldierRadius)
			
			love.graphics.setColor(unit.colourR, unit.colourG, unit.colourB, a)
			love.graphics.line(interpolatedPos.x, interpolatedPos.y, interpolatedPos.x + dirVector.x, interpolatedPos.y + dirVector.y)
			love.graphics.setColor(255, 255, 255, a)]]
			
			love.graphics.pop()
		end
		
	else
	
		local animQuad
		
		for i = 1, #self.renderList do
			local id = self.renderList[i][2]
			local unit = self.unitList[ self.renderList[i][1] ]
			
			local sheet, animQuad, offset = unit.animator:getAnimFromID(id)

			local lastFramePos = self.lastFramePositions[self.renderList[i][1]][id]
			
			-- We always render at least 1 frame in the past
			local stepSize = self.currentSimTime - self.lastSimTime
			while self.renderTime - stepSize > self.currentSimTime do
				self.renderTime = self.renderTime - stepSize
			end
			
			local interpTime = (self.renderTime - stepSize - self.lastSimTime) / stepSize -- 0 when matching past frame frame, 1 when matching current frame
			local interpolatedPos = lerp(lastFramePos, unit.soldierPos[id], interpTime)
			
			-- We scale the y position to make it look isometric
			-- Note that the image itself isn't scaled like when using love.graphics.scale, as the soldiers are already drawn isometrically
			local adjustedPos = interpolatedPos
			adjustedPos.y = adjustedPos.y * yScale
			
			love.graphics.draw(sheet, animQuad, adjustedPos.x, adjustedPos.y, 0, 0.2, 0.2, offset.x, offset.y)
		end
		
	end
end