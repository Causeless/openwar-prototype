require "MathsTools"
require "VectorTools"
class = require "hump.class"
vec2 = require "hump.vector"

-- Todo: handle dead soldiers when implemented, check for zero length lines

SlotAssignmentStrategy = class{}

function SlotAssignmentStrategy:assignTargetsElimination(unit, inTargets)	-- Todo: throw error on target's length != soldier count
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local openTargetsList = {}
	local openSoldiersList = {}
	local soldiersLeft = unit.soldierCount
	
	local closestSoldiers = {}
	
	local soldierTargetPairs = {}
	
	-- Fill the open lists with all targets and soldiers
	for i = 1, unit.soldierCount do
		table.insert(openTargetsList, i)
		table.insert(openSoldiersList, i)
	end
	
	local bias = vec2(1, 1)
	
	while soldiersLeft > 0 do -- While soldiers are still looking for a formation slot...
		closestSoldiers = {} -- Clear our closest soldiers list
	
		for key, soldier in pairs(openSoldiersList) do -- For each soldier that hasn't found a formation slot
			local closestDistance = 1/0
			local closestID = nil
			
			for key, target in pairs(openTargetsList) do -- For each open target
				distance = (unit.soldierPos[soldier] - targets[target].pos):len2()
				if distance < closestDistance then
					closestDistance = distance
					closestID = target
				end
			end
			
			if closestSoldiers[closestID] == nil then
				closestSoldiers[closestID] = {}
			end
			table.insert(closestSoldiers[closestID], soldier) -- Insert myself into list of soldiers which are closest to this target
		end
		
		local freeToPick = true
		for target, soldierList in pairs(closestSoldiers) do
			if #soldierList > 1 then
				freeToPick = false
			end
		end
		
		for target, soldierList in pairs(closestSoldiers) do
			for key, soldier in ipairs(soldierList) do -- Remove all soldiers that aren't in open list, so future targets in this iteration do not consider them
				if not openSoldiersList[soldier] then
					table.remove(soldierList, key)
				end
			end
			
			if #soldierList > 1 then -- If multiple soldiers are competing for this formation slot
				local furthestID = nil
				local furthestSoldierListID = nil
				local furthestDistance = 0.0
				for i = 1, #soldierList do -- Pick furthest soldier
					distance = (unit.soldierPos[soldierList[i]] - targets[target].pos):len2()
					if distance > furthestDistance then
						furthestDistance = distance
						furthestID = soldierList[i]
						furthestSoldierListID = i
					end
				end
				soldierTargetPairs[furthestID] = target -- Set this soldier to have this target
				
				-- Remove soldier and chosen target from open list
				openTargetsList[target] = nil
				openSoldiersList[furthestID] = nil
				soldiersLeft = soldiersLeft - 1
			elseif #soldierList == 1 and freeToPick then
				soldierTargetPairs[soldierList[1]] = target
				
				-- Remove soldier and chosen target from open list
				openTargetsList[target] = nil
				openSoldiersList[soldierList[1]] = nil
				soldiersLeft = soldiersLeft - 1
			end
		end
	end
	
	return soldierTargetPairs
	
	-- perhaps handle situation when 2 targets are equal distance?
end

--[[function SlotAssignmentStrategy:assignTargetsAxisSort(unit, inTargets)	-- Todo: throw error on target's length != soldier count	
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local markedTaken = {}
	
	local soldierTargetPairs = {}
	
	local unitPosition = unit:findAveragePosition()
	
	local targetPosition = vec2(0, 0)
	for i = 1, unit.soldierCount do
		targetPosition = targetPosition + targets[i].pos
	end
	targetPosition = targetPosition / unit.soldierCount	-- Find average position of all targets
	
	local vecToUnit = targetPosition - unitPosition
	local push = setLength(vecToUnit, -5000)  -- Give a big push away to ensure that the edge isn't inside the formation
	--local push = vecToUnit
	local farEdgeStart = push
	local farEdgeEnd = push + vecToUnit:perpendicular()
	
	local sortedCount = 1  -- Start at 1 because lua arrays start at 1...
	
	for i = 1, unit.soldierCount do  -- For each target...
		local currentMax = 0
		local currentMaxIndex = sortedCount
	
		for j = sortedCount, unit.soldierCount do  -- For remaining targets...
			local pathLength = minDistanceToLine(farEdgeStart, farEdgeEnd, targets[j].pos)
			if pathLength > currentMax then
				currentMax = pathLength;
				currentMaxIndex = j;
			end
		end

		local temp = targets[sortedCount].pos
		targets[sortedCount].pos = targets[currentMaxIndex].pos
		targets[currentMaxIndex].pos = temp

		sortedCount = sortedCount + 1
	end
	
	for i = 1, unit.soldierCount do	-- For each target
		local prevLen = 1/0 -- Set to infinity
		local closestSoldier = nil
		
		for j = 1, unit.soldierCount do	-- For each soldier
			if not markedTaken[j] then
				local pathLength = (targets[i].pos - unit.soldierPos[j]):len2()
				if pathLength < prevLen then
					prevLen = pathLength
					closestSoldier = j
				end
			end
		end
		
		markedTaken[closestSoldier] = true
		
		soldierTargetPairs[closestSoldier] = i
	end
	
	return soldierTargetPairs
end]]

function SlotAssignmentStrategy:assignTargetsAxisSort(unit, inTargets)	-- Todo: throw error on target's length != soldier count	
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local markedTaken = {}
	
	local soldierTargetPairs = {}
	
	local unitPosition = unit:findAveragePosition()
	
	local targetPosition = vec2(0, 0)
	for i = 1, unit.soldierCount do
		targetPosition = targetPosition + targets[i].pos
	end
	targetPosition = targetPosition / #targets	-- Find average position of all targets
	
	local vecToUnit = targetPosition - unitPosition
	local push = setLength(vecToUnit, -5000)  -- Give a big push away to ensure that the edge isn't inside the formation
	--local push = vecToUnit
	local farEdgeStart = push
	local farEdgeEnd = push + vecToUnit:perpendicular()
	
	local sortedCount = 1  -- Start at 1 because lua arrays start at 1...
	
	for i = 1, #targets do  -- For each target...
		local currentMax = 0
		local currentMaxIndex = sortedCount
	
		for j = sortedCount, #targets do  -- For remaining targets...
			local pathLength = minDistanceToLine(farEdgeStart, farEdgeEnd, targets[j].pos)
			if pathLength > currentMax then
				currentMax = pathLength;
				currentMaxIndex = j;
			end
		end

		local temp = targets[sortedCount]
		targets[sortedCount] = targets[currentMaxIndex]
		targets[currentMaxIndex] = temp
		
		temp = targets[sortedCount].pos
		targets[sortedCount].pos = targets[currentMaxIndex].pos
		targets[currentMaxIndex].pos = temp

		sortedCount = sortedCount + 1
	end
	
	for i = 1, #targets do	-- For each target
		local prevLen = 1/0 -- Set to infinity
		local closestSoldier = nil
		
		for j = 1, unit.soldierCount do	-- For each soldier
			if not markedTaken[j] then
				local pathLength = (targets[i].pos - unit.soldierPos[j]):len2()
				if pathLength < prevLen then
					prevLen = pathLength
					closestSoldier = j
				end
			end
		end
		
		markedTaken[closestSoldier] = true
		
		soldierTargetPairs[closestSoldier] = i
	end
	
	return soldierTargetPairs
end

function SlotAssignmentStrategy:assignTargetsDistanceSort(unit, inTargets)	-- Todo: throw error on target's length != soldier count
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local markedTaken = {}
	local distanceFromFormation = {}
	
	local soldierTargetPairs = {}
	
	local soldierIterationOrder = {}
	for i = 1, unit.soldierCount do
		table.insert(soldierIterationOrder, i)
	end
	
	-- For each soldier, find the minimum distance to any formation slot
	-- Make this the distanceFromFormation
	-- Later sort by distanceFromFormation, so furthest away soldiers choose targets first
	
	for soldier = 1, unit.soldierCount do -- For each soldier
		local minDistance = 1/0 -- Set to infinity
		for target = 1, unit.soldierCount do -- For each target
			local pathLength = (unit.soldierPos[soldier] - targets[target].pos):len2()
			if pathLength < minDistance then
				minDistance = pathLength;
			end
		end
		distanceFromFormation[soldier] = minDistance
	end
	
	function sortFunction(a, b)
		return distanceFromFormation[a] > distanceFromFormation[b];
	end
	
	table.sort(soldierIterationOrder, sortFunction)
	
	for key, soldier in ipairs(soldierIterationOrder) do	-- For each soldier
		local prevLen = 1/0 -- Set to infinity
		local closestTarget = nil
		
		--print(tostring(soldier))
		
		for target, targetPos in ipairs(targets) do	-- For each target
			if not markedTaken[target] then
				local pathLength = (targets[target].pos - unit.soldierPos[soldier]):len2()
				if pathLength < prevLen then
					prevLen = pathLength
					closestTarget = target
				end
			end
		end
		
		markedTaken[closestTarget] = true
		
		soldierTargetPairs[soldier] = closestTarget
	end
	
	return soldierTargetPairs
end

function SlotAssignmentStrategy:assignTargetsManhattenDistanceSort(unit, inTargets)	-- Todo: throw error on target's length != soldier count
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local markedTaken = {}
	local distanceFromFormation = {}
	
	local soldierTargetPairs = {}
	
	local soldierIterationOrder = {}
	for i = 1, unit.soldierCount do
		table.insert(soldierIterationOrder, i)
	end
	
	-- For each soldier, find the minimum distance to any formation slot
	-- Make this the distanceFromFormation
	-- Later sort by distanceFromFormation, so furthest away soldiers choose targets first
	
	for soldier = 1, unit.soldierCount do -- For each soldier
		local minDistance = 1/0 -- Set to infinity
		for target = 1, unit.soldierCount do -- For each target
			local pathLength = unit.soldierPos[soldier].x + targets[target].pos.x + unit.soldierPos[soldier].y + targets[target].pos.y
			if pathLength < minDistance then
				minDistance = pathLength;
			end
		end
		distanceFromFormation[soldier] = minDistance
	end
	
	function sortFunction(a, b)
		return distanceFromFormation[a] > distanceFromFormation[b];
	end
	
	table.sort(soldierIterationOrder, sortFunction)
	
	for key, soldier in ipairs(soldierIterationOrder) do	-- For each soldier
		local prevLen = 1/0 -- Set to infinity
		local closestTarget = nil
		
		--print(tostring(soldier))
		
		for target, targetPos in ipairs(targets) do	-- For each target
			if not markedTaken[target] then
				local pathLength = (targets[target].pos - unit.soldierPos[soldier]):len2()
				if pathLength < prevLen then
					prevLen = pathLength
					closestTarget = target
				end
			end
		end
		
		markedTaken[closestTarget] = true
		
		soldierTargetPairs[soldier] = closestTarget
	end
	
	return soldierTargetPairs
end

-- Todo: try having soldiers select targets instead of the opposite?
function SlotAssignmentStrategy:assignTargetsCentreOut(unit, inTargets)	-- Todo: throw error on target's length != soldier count
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local centre = vec2(0, 0)
	for i = 1, unit.soldierCount do
		centre = centre + targets[i].pos
	end
	centre = centre / #targets	-- Find average position of all targets
	
	local markedTaken = {}
	local distanceFromCentre = {}
	
	local soldierTargetPairs = {}
	
	local targetIterationOrder = {}
	for i = 1, #targets do
		table.insert(targetIterationOrder, i)
	end
	
	-- For each target, find the distance to our centre of mass
	-- Make this the distanceFromCentre
	-- Later sort by distanceFromCentre, so central targets choose soldiers first
	
	for target = 1, #targets do -- For each target
		local pathLength = (centre - targets[target].pos):len2()
		
		distanceFromCentre[target] = pathLength
	end
	
	function sortFunction(a, b)
		return distanceFromCentre[a] < distanceFromCentre[b];
	end
	
	table.sort(targetIterationOrder, sortFunction)
	
	for key, targetID in ipairs(targetIterationOrder) do	-- For each target
		local prevLen = 1/0 -- Set to infinity
		local closestSoldier = nil
		
		for soldierID = 1, unit.soldierCount do	-- For each soldier
			if not markedTaken[soldierID] then
				local pathLength = (targets[targetID].pos - unit.soldierPos[soldierID]):len2()
				if pathLength < prevLen then
					prevLen = pathLength
					closestSoldier = soldierID
				end
			end
		end
		
		markedTaken[closestSoldier] = true
		
		soldierTargetPairs[closestSoldier] = targetID
	end
	
	return soldierTargetPairs
end

function SlotAssignmentStrategy:assignTargetsEdgeIn(unit, inTargets)	-- Todo: throw error on target's length != soldier count
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local centre = vec2(0, 0)
	for i = 1, unit.soldierCount do
		centre = centre + targets[i].pos
	end
	centre = centre / #targets	-- Find average position of all targets
	
	local markedTaken = {}
	local distanceFromCentre = {}
	
	local soldierTargetPairs = {}
	
	local targetIterationOrder = {}
	for i = 1, #targets do
		table.insert(targetIterationOrder, i)
	end
	
	-- For each target, find the distance to our centre of mass
	-- Make this the distanceFromCentre
	-- Later sort by distanceFromCentre, so central targets choose soldiers first
	
	for target = 1, #targets do -- For each target
		local pathLength = (centre - targets[target].pos):len2()
		
		distanceFromCentre[target] = pathLength
	end
	
	function sortFunction(a, b)
		return distanceFromCentre[a] > distanceFromCentre[b];
	end
	
	table.sort(targetIterationOrder, sortFunction)
	
	for key, targetID in ipairs(targetIterationOrder) do	-- For each target
		local prevLen = 1/0 -- Set to infinity
		local closestSoldier = nil
		
		for soldierID = 1, unit.soldierCount do	-- For each soldier
			if not markedTaken[soldierID] then
				local pathLength = (targets[targetID].pos - unit.soldierPos[soldierID]):len2()
				if pathLength < prevLen then
					prevLen = pathLength
					closestSoldier = soldierID
				end
			end
		end
		
		markedTaken[closestSoldier] = true
		
		soldierTargetPairs[closestSoldier] = targetID
	end
	
	return soldierTargetPairs
end

function SlotAssignmentStrategy:assignTargetsLayered(unit, inTargets) -- TODO actually write this...
	local targets = {}
	for i = 1, #inTargets do
		targets[i] = inTargets[i]
	end
	
	local layerWidth = 15 -- Just some constant determining how wide each layer is
	local layerTargets = {}
	
	local soldierTargetPairs = {}
	
	function sortFunction(a, b)
		return a.pos.x < b.pos.x;
	end
	
	table.sort(targets, sortFunction)
	
	for i = 1, #targets do

	
		soldierTargetPairs[i] = i
	end
	
	return soldierTargetPairs
end

function SlotAssignmentStrategy:assignTargetsID(unit, inTargets)
	local soldierTargetPairs = {}
	
	for i = 1, unit.soldierCount do
		soldierTargetPairs[i] = i
	end
	
	return soldierTargetPairs
end

-- Pretty sure the algorithm is actually broken. FIX IT
-- It seems to be mirrored along an axis sometimes?
function SlotAssignmentStrategy:assignTargetsStableMarriage(unit, inTargets)
	local soldierEngagedTarget = {}
	local targetEngagedSoldier = {}
	
	local soldierTargetProposals = {}
	
	for i = 1, unit.soldierCount do
		-- Initialize every soldier to have no engaged target (-1)
		soldierEngagedTarget[i] = -1
		
		-- And vice versa
		targetEngagedSoldier[i] = -1
		
		-- And set the proposals to nothing
		soldierTargetProposals[i] = {}
	end
	
	local freeSoldier = function()
		for i = 1, #soldierEngagedTarget do
			if soldierEngagedTarget[i] == -1 then
				return i
			end
		end
		
		return -1
	end
	
	local getTopTargetList = function(soldierID)
		local targets = {}
		for i = 1, #inTargets do
			targets[i] = i
		end
		
		local distanceSort = function(a, b)
			local aTarget = inTargets[a]
			local bTarget = inTargets[b]

			local aDist = (aTarget.pos - unit.soldierPos[soldierID]):len2()
			local bDist = (bTarget.pos - unit.soldierPos[soldierID]):len2()
			
			return aDist < bDist
		end
		
		table.sort(targets, distanceSort)
		
		return targets
	end
	
	local getTopNonProposedTarget = function(soldierID)
		local targetList = getTopTargetList(soldierID)
		for i = 1, #targetList do
			local targetID = targetList[i]
			if not soldierTargetProposals[soldierID][targetID] then
				return targetID
			end
		end
		
		return -1
	end

	local propose = function(soldierID, targetID)
		soldierTargetProposals[soldierID][targetID] = true
	end

	local engage = function(soldierID, targetID)
		soldierEngagedTarget[soldierID] = targetID
		targetEngagedSoldier[targetID] = soldierID
	end
	
	while true do
		local freeSoldier = freeSoldier()
		if freeSoldier == -1 then break end
		
		local topTarget = getTopNonProposedTarget(freeSoldier)
		if topTarget ~= -1 then
			propose(freeSoldier, topTarget)

			if targetEngagedSoldier[topTarget] == -1 then
				engage(freeSoldier, topTarget)
			else
				local otherSoldier = targetEngagedSoldier[topTarget]

				local freeSoldierDistance = (inTargets[topTarget].pos - unit.soldierPos[freeSoldier]):len2()
				local otherSoldierDistance = (inTargets[topTarget].pos - unit.soldierPos[otherSoldier]):len2()
				
				-- This should be <
				-- But works better as >?
				if freeSoldierDistance < otherSoldierDistance then
					soldierEngagedTarget[otherSoldier] = -1
					engage(freeSoldier, topTarget)
				end
			end
		end
	end
	
	return soldierEngagedTarget
end