class = require "hump.class"
vec2 = require "hump.vector"
require "VectorTools"

SteeringManager = class{}

function SteeringManager:init(host)
    self.host = host	-- Host must be of type Unit
	
	self.checkedUnits = {host}	-- Table to store other units to be checked against, for example for soldier avoidance. Includes self, as soldiers must avoid other soldiers in same unit
	
	self.seperationRadius = 7	-- What distance until the soldier attempts to move away?
	self.seperationRadiusSqr = self.seperationRadius * self.seperationRadius	-- Use squared distance for performance improvements
	self.foreignSeperationRadius = 12	-- Sep distance for soldiers to tolerate a bigger/smaller separation distance for non-host units
	self.foreignSeperationRadiusSqr = self.foreignSeperationRadius * self.foreignSeperationRadius
	
	self.seperationStrength = 0.5	-- How strongly does the soldier attempt to move away?
	self.foreignSeperationStrength = 0.1	-- Sep strength for soldiers to separate away from non-host units more/less
	
	self.alignRadius = 15	-- What distance until the soldier attempts to align velocity
	self.alignRadiusSqr = self.alignRadius * self.alignRadius
	self.foreignAlignRadius = 15
	self.foreignAlignRadiusSqr = self.foreignAlignRadius * self.foreignAlignRadius
	
	self.alignStrength = 0.0
	self.foreignAlignStrength = -0.1
	
	self.avoidRadius = 0	-- Circle radius around soldier to avoid
	self.avoidRadiusSqr = self.avoidRadius * self.avoidRadius
	self.foreignAvoidRadius = 9
	self.foreignAvoidRadiusSqr = self.foreignAvoidRadius * self.foreignAvoidRadius
	
	self.avoidSeeAheadAmount = 0	-- How far to look ahead
	self.foreignAvoidSeeAheadAmount = 8.0
	
	self.avoidStrength = 0
	self.foreignAvoidStrength = 120.0
	
	self.divertRadius = 8	-- Circle radius around soldier to avoid
	self.divertRadiusSqr = self.divertRadius * self.divertRadius
	self.foreignDivertRadius = 10
	self.foreignDivertRadiusSqr = self.foreignDivertRadius * self.foreignDivertRadius
	
	self.divertSeeAheadAmount = 4	-- How far to look ahead
	self.foreignDivertSeeAheadAmount = 4
	
	self.divertMaxCollisionTime = 1 -- Max time before ignoring pending collisions
	self.foreignDivertMaxCollisionTime = 1.5
	
	self.divertStrength = 25
	self.foreignDivertStrength = 30
	
	self.seekStrength = 1.0
	
	self.checks = {}
end

function SteeringManager:seekPoint(target)
	--local unitForces = {}
	for i = 1, self.host.soldierCount do
		local steering = (target - self.host.soldierPos[i]) * self.seekStrength	-- Target is path from soldier to target
		steering = setLength(steering, self.host.runSpeed)  -- Set to max vel
		
		steering = steering - self.host.soldierVel[i]	-- Take current vel into account
		self.host:addForce(i, truncate(steering, self.host.maxAccel))
		--table.insert(unitForces, truncate(steering, self.host.maxAccel))	-- And limit
	end
	
	--self.host:addAllForces(unitForces)    -- The GC doesn't like deallocating these extra tables
end

function SteeringManager:seekPoints(targets, soldierTargetPairs)	-- Todo: throw error on target's length != soldier count
	-- Perform steering
	for soldier, target in ipairs(soldierTargetPairs) do
		local vecToTarget = targets[target].pos - self.host.soldierPos[soldier]
		local steering = vecToTarget * self.seekStrength	-- Target is path from soldier to target
		
		self.host:addForce(soldier, steering)
		
		if self.host.soldierVel[soldier]:len2() < 3*3 and vecToTarget:len2() < 3*3 then
			self.host.soldierDir[soldier] = targets[target].dir
		end
	end
end

function SteeringManager:separate()
	--local unitForces = {}
	
	self.checks = {}
	
	local foreignSteering = vec2(0, 0)
	local friendSteering = vec2(0, 0)
	local seperationPath = vec2(0, 0)
	local distance = 0
	
	local maxRadius = 0
	
	local setLengthInplace_local = setLengthInplace
	
	for i = 1, self.host.soldierCount do	-- For every soldier in host unit
		foreignSteering = vec2(0, 0)
		friendSteering = vec2(0, 0)
		
		for keyUnit, unit in ipairs(self.checkedUnits) do	-- For every checked unit
			if unit == self.host then
				maxRadius = self.seperationRadius
			else
				maxRadius = self.foreignSeperationRadius
			end
			
			local potentiallyCollidingSet = unit.grid:getTableForCells(self.host.soldierPos[i], maxRadius)
			
			for keyID, soldierID in ipairs(potentiallyCollidingSet) do	-- For every soldier in grid cells
				distance = self.host.soldierPos[i]:dist2(unit.soldierPos[soldierID])
				
				table.insert(self.checks, {self.host.soldierPos[i], unit.soldierPos[soldierID]})
				if unit == self.host and distance < self.seperationRadiusSqr and distance > 0 then
					seperationPath = (self.host.soldierPos[i] - unit.soldierPos[soldierID])
					
					seperationPath:normalize_inplace()
					seperationPath = seperationPath / distance
					
					friendSteering = friendSteering + seperationPath
				elseif unit ~= self.host and distance < self.foreignSeperationRadiusSqr and distance > 0 then
					seperationPath = (self.host.soldierPos[i] - unit.soldierPos[soldierID])
					
					seperationPath:normalize_inplace()
					seperationPath = seperationPath / distance
					
					foreignSteering = foreignSteering + seperationPath
				end
			end
		end
		
		setLengthInplace_local(friendSteering, (self.host.maxAccel * self.seperationStrength))
		setLengthInplace_local(foreignSteering, (self.host.maxAccel * self.foreignSeperationStrength))
		
		steering = friendSteering + foreignSteering
		--steering = steering - self.host.soldierVel[i]	-- Try to slow down, calm jittering
		self.host:addForce(i, steering)
		--table.insert(unitForces, truncate(steering, self.host.maxAccel))
	end

	--self.host:addAllForces(unitForces)	-- The GC doesn't like deallocating these extra tables
end

function SteeringManager:align()
	--local unitForces = {}
	
	local foreignSteering = vec2(0, 0)
	local friendSteering = vec2(0, 0)
	local alignPath = vec2(0, 0)
	local distance = 0
	
	local maxRadius = 0
	
	local setLengthInplace_local = setLengthInplace
	
	for i = 1, self.host.soldierCount do	-- For every soldier in host unit
		foreignSteering = vec2(0, 0)
		friendSteering = vec2(0, 0)
		
		for keyUnit, unit in ipairs(self.checkedUnits) do	-- For every checked unit
			if unit == self.host then
				maxRadius = self.alignRadius
			else
				maxRadius = self.foreignAlignRadius
			end
		
			local potentiallyCollidingSet = unit.grid:getTableForCells(self.host.soldierPos[i], maxRadius)
		
			for keyID, soldierID in ipairs(potentiallyCollidingSet) do	-- For every soldier in grid cells
				distance = self.host.soldierPos[i]:dist2(unit.soldierPos[soldierID])
				if unit == self.host and distance < self.alignRadiusSqr and distance > 0 then
					alignPath = unit.soldierVel[soldierID]

					alignPath = alignPath / distance
					
					friendSteering = friendSteering + alignPath
				elseif unit ~= self.host and distance < self.foreignAlignRadiusSqr and distance > 0 then
					alignPath = unit.soldierVel[soldierID]

					alignPath = alignPath / distance
					
					foreignSteering = foreignSteering + alignPath
				end
			end
		end
		
		setLengthInplace_local(friendSteering, (self.host.maxAccel * self.alignStrength))
		setLengthInplace_local(foreignSteering, (self.host.maxAccel * self.foreignAlignStrength))
		
		steering = friendSteering + foreignSteering
		steering = steering - self.host.soldierVel[i]	-- Try to slow down, calm jittering
		steering = steering / 8
		self.host:addForce(i, steering)
		--table.insert(unitForces, truncate(steering, self.host.maxAccel))
	end

	--self.host:addAllForces(unitForces)	-- The GC doesn't like deallocating these extra tables
end

function SteeringManager:avoid()
	--local unitForces = {}
	
	local foreignSteering = vec2(0, 0)
	local friendSteering = vec2(0, 0)
	local avoidPath = vec2(0, 0)
	local dynamicLength = 0
	local ahead = vec2(0,0)
	local foreignAhead = vec2(0, 0)
	local circlePos = vec2(0, 0)
	local circleRad = 0
	
	local maxRadius = 0
	
	local setLength_local = setLength -- Use as a local function to speed up calls
	local setLengthInplace_local = setLengthInplace
	
	for i = 1, self.host.soldierCount do	-- For every soldier in host unit
		foreignSteering = vec2(0, 0)
		friendSteering = vec2(0, 0)
		
		for keyUnit, unit in ipairs(self.checkedUnits) do	-- For every checked unit
			if unit == self.host then
				maxRadius = self.avoidSeeAheadAmount + self.avoidRadius
			else
				maxRadius = self.foreignAvoidSeeAheadAmount + self.foreignAvoidRadius
			end
		
			local potentiallyCollidingSet = unit.grid:getTableForCells(self.host.soldierPos[i], maxRadius)
		
			for keyID, soldierID in ipairs(potentiallyCollidingSet) do	-- For every soldier in checked unit and host unit
				dynamicLength = self.host.soldierVel[i]:len() / self.host.runSpeed
				--ahead = self.host.soldierPos[i] + setLength(self.host.soldierVel[i], self.avoidSeeAheadAmount)
				--foreignAhead = self.host.soldierPos[i] + setLength(self.host.soldierVel[i], self.foreignAvoidSeeAheadAmount)
				ahead = self.host.soldierPos[i] + setLength_local(self.host.soldierVel[i], dynamicLength * self.avoidSeeAheadAmount)
				foreignAhead = self.host.soldierPos[i] + setLength_local(self.host.soldierVel[i], dynamicLength * self.foreignAvoidSeeAheadAmount)
				
				circlePos = unit.soldierPos[soldierID]
				
				if unit == self.host and lineIntersectsCircle2(self.host.soldierPos[i], ahead, circlePos, self.avoidRadiusSqr) and soldierID ~= i then
					avoidPath = ahead - circlePos
					setLengthInplace_local(avoidPath, self.avoidStrength)
					
					friendSteering = friendSteering + avoidPath
				elseif unit ~= self.host and lineIntersectsCircle2(self.host.soldierPos[i], foreignAhead, circlePos, self.foreignAvoidRadiusSqr) then
					avoidPath = ahead - circlePos -- For some reason just using normal ahead instead of foreign ahead works better here
					setLengthInplace_local(avoidPath, self.foreignAvoidStrength)
					
					foreignSteering = foreignSteering + avoidPath
				end
			end
		end
		
		steering = friendSteering + foreignSteering
		steering = steering - self.host.soldierVel[i]	-- Try to slow down, calm jittering
		self.host:addForce(i, steering)
		--table.insert(unitForces, truncate(steering, self.host.maxAccel))
	end

	--self.host:addAllForces(unitForces)	-- The GC doesn't like deallocating these extra tables
end

function SteeringManager:divert()
	for i = 1, self.host.soldierCount do
		local shortestTime = 1/0
		
		local firstTarget = nil
		local firstMaxRadius, firstUnit;
		local firstMinSeperation, firstDistance, firstRelativePos, firstRelativeVel
		
		local timeToCollision, minSeperation, distance, relativePos, relativeVel, relativeSpeed
		
		local maxRadius, maxCollisionTime, avoidRadius
		
		-- Measures how much responsibility this soldier had to move.
		-- A static soldier shouldn't try to avoid collisions, but a moving soldier should
		-- So this gives relative higher move priority to the soldier who is moving faster
		local movePriority
		
		for keyUnit, unit in ipairs(self.checkedUnits) do	-- For every checked unit
			if unit == self.host then
				maxRadius = self.divertSeeAheadAmount
				maxCollisionTime = self.divertMaxCollisionTime
			else
				maxRadius = self.foreignDivertSeeAheadAmount
				maxCollisionTime = self.foreignDivertMaxCollisionTime
			end
			
			local potentiallyCollidingSet = unit.grid:getTableForCells(self.host.soldierPos[i], maxRadius)
		
			for keyID, soldierID in ipairs(potentiallyCollidingSet) do
				if unit ~= self.host or soldierID ~= i then
					-- Calculate time to collision
					relativePos = unit.soldierPos[soldierID] - self.host.soldierPos[i]
					relativeVel = unit.soldierVel[soldierID] - self.host.soldierVel[i]
					relativeSpeed = relativeVel:len()
					timeToCollision = -(relativePos:dot(relativeVel) / (relativeSpeed * relativeSpeed))
						
					-- Check whether a collision will actually occur at all
					distance = relativePos:len()
					minSeperation = distance - relativeSpeed * timeToCollision

					if minSeperation <= self.divertRadius*0.5 and 
						timeToCollision > 0 and timeToCollision < shortestTime and
						timeToCollision < maxCollisionTime then
						
						shortestTime = timeToCollision
						firstTarget = soldierID
						firstMinSeperation = minSeperation
						firstDistance = distance
						firstRelativePos = relativePos
						firstRelativeVel = relativeVel
						firstUnit = unit;
					end
				end
			end
		end
		
		if firstTarget then
			if firstMinSeperation <= 0 or firstDistance < self.divertRadius*0.5 then
				relativePos = firstUnit.soldierPos[firstTarget] - self.host.soldierPos[i]
			else
				relativePos = firstRelativePos + firstRelativeVel * shortestTime
			end
			
			local steering
			
			local myVel = self.host.soldierVel[i]:len2()
			local targetVel = firstUnit.soldierVel[firstTarget]:len2()
			
			local combinedVel = myVel + targetVel
			movePriority = myVel / combinedVel -- No need to check for division by 0, as the earlier checks for timeToCollision wouldn't pass otherwise
			
			if firstUnit == self.host then
				steering = -relativePos * self.divertStrength * movePriority
			else
				steering = -relativePos * self.foreignDivertStrength * movePriority
			end
			
			self.host:addForce(i, steering)
		end
	end
end

--[[function SteeringManager:avoid()
	for i = 1, self.host.soldierCount do
		local shortestTime = 1/0
		
		local atLeastOneTarget = false
		local combinedSteering = vec2(0, 0);
		
		local timeToCollision, minSeperation, distance, relativePos, relativeVel, relativeSpeed
		
		local maxRadius, maxCollisionTime, avoidRadius
		
		-- Measures how much responsibility this soldier had to move.
		-- A static soldier shouldn't try to avoid collisions, but a moving soldier should
		-- So this gives relative higher move priority to the soldier who is moving faster
		local movePriority
		
		for keyUnit, unit in ipairs(self.checkedUnits) do	-- For every checked unit
			if unit == self.host then
				maxRadius = self.avoidSeeAheadAmount
				maxCollisionTime = self.avoidMaxCollisionTime
			else
				maxRadius = self.foreignAvoidSeeAheadAmount
				maxCollisionTime = self.foreignAvoidMaxCollisionTime
			end
			
			local potentiallyCollidingSet = unit.grid:getTableForCells(self.host.soldierPos[i], maxRadius)
		
			for keyID, soldierID in ipairs(potentiallyCollidingSet) do
				if unit ~= self.host or soldierID ~= i then
					-- Calculate time to collision
					relativePos = unit.soldierPos[soldierID] - self.host.soldierPos[i]
					relativeVel = unit.soldierVel[soldierID] - self.host.soldierVel[i]
					relativeSpeed = relativeVel:len()
					timeToCollision = -(relativePos:dot(relativeVel) / (relativeSpeed * relativeSpeed))
					--print(timeToCollision)
						
					-- Check whether a collision will actually occur at all
					distance = relativePos:len()
					minSeperation = distance - relativeSpeed*timeToCollision
					
					local myVel = self.host.soldierVel[i]:len()
					local targetVel = unit.soldierVel[soldierID]:len()
			
					local combinedVel = myVel + targetVel
					movePriority = myVel / combinedVel

					if minSeperation <= self.avoidRadius * movePriority and 
						timeToCollision > 0 and
						timeToCollision < maxCollisionTime then
						
						atLeastOneTarget = true
						
						if minSeperation <= 0 or distance < (self.avoidRadius * 2) then
							relativePos = unit.soldierPos[soldierID] - self.host.soldierPos[i]
						else
							relativePos = relativePos + relativeVel * timeToCollision
						end
						
						if combinedUnit == self.host then
							combinedSteering = combinedSteering + setLength(-relativePos, self.avoidStrength)
						else
							combinedSteering = combinedSteering + setLength(-relativePos, self.foreignAvoidStrength)
						end
					end
				end
			end
		end
		
		if atLeastOneTarget then
			self.host:addForce(i, combinedSteering)
		end
	end
end]]

function SteeringManager:addCheckedUnit(unit)
	return table.insert(self.checkedUnits, unit)
end

function SteeringManager:removedCheckedUnit(unitID)
	table.remove(unitID)
end