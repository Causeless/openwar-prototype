class = require "hump.class"
vec2 = require "hump.vector"
require "VectorTools"
require "MathsTools"

AnimationManager = class{}

function AnimationManager:init(unit, sheetPath)	
	self.host = unit
	self.sheetPath = sheetPath
	
	-- Lookup table to convert a vector to an image (for finding correct directions etc)
	self.lookup = { vec2(0, 1), vec2(-1, 1), vec2(-1, 0), vec2(-1, -1), vec2(0, -1), vec2(1, -1), vec2(1, 0), vec2(1, 1) }
	for i = 1, #self.lookup do
		self.lookup[i]:normalize_inplace() -- Normalize each element to make direction checking more accurate
	end
	
	self.animTimelines = {}
	for i = 1, self.host.soldierCount do
		self.animTimelines[i] = 0.0 -- Start all animation timelines at 0
	end
	
	self:initIdle()
	self:initWalk()
	self:initBlock()
end

function AnimationManager:initIdle()
	self.idleAnim = {
		sheets = {},
		frameCount = 0,
		imgWidth = 0,
		quads = {},
		offset = vec2(0, 0)
	}
	
	self.idleAnim.sheets[1] = love.graphics.newImage(self.sheetPath .. "_Idle_strip8.png")
	self.idleAnim.frameCount = 8
	self.idleAnim.imgWidth = self.idleAnim.sheets[1]:getWidth() / self.idleAnim.frameCount
	
	local anim = self.idleAnim
	for i = 1, anim.frameCount do
		local j = i - 1 -- To simplify things since lua arrays count from 1...
		-- We assume that all sheets are the same dimensions
		anim.quads[i] = love.graphics.newQuad(j * anim.imgWidth, 0, anim.imgWidth, anim.sheets[1]:getHeight(), anim.sheets[1]:getDimensions())
	end -- Splits the sheet into quads of 1 image each
	
	self.idleAnim.offset = vec2(112, 119)
end

function AnimationManager:getSheet_Idle()
	return self.idleAnim.sheets[1] -- Idle anim only uses 1 sheet. We use anim quads for directions instead of sheets
end

function AnimationManager:getAnim_Idle(soldierID)
	local dirVector = vec2(math.cos(self.host.soldierDir[soldierID]), math.sin(self.host.soldierDir[soldierID]))
	
	local sheet = self:getSheet_Idle()
	
	local shortestDistance = 1/0 -- Set to infinity
	local shortestIndex = nil
	
	for i = 1, 8 do
		local distance = (dirVector - self.lookup[i]):len2()
		if distance < shortestDistance then
			shortestDistance = distance
			shortestIndex = i
		end
	end
	
	return sheet, self.idleAnim.quads[shortestIndex]
end

function AnimationManager:initWalk()
	self.walkAnim = {
		sheets = {},
		frameCount = 0,
		imgWidth = 0,
		quads = {},
		offset = vec2()
	}
	
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_Down_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_DownL_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_Left_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_UpL_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_Up_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_UpR_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_Right_strip10.png"))
	table.insert(self.walkAnim.sheets, love.graphics.newImage(self.sheetPath .. "_walk/_DownR_strip10.png"))
	
	self.walkAnim.frameCount = 10
	
	self.walkAnim.imgWidth = self.walkAnim.sheets[1]:getWidth() / self.walkAnim.frameCount
	
	local anim = self.walkAnim
	for i = 1, anim.frameCount do
		local j = i - 1 -- To simplify things since lua arrays count from 1...
		-- We assume that all sheets are the same dimensions
		anim.quads[i] = love.graphics.newQuad(j * anim.imgWidth, 0, anim.imgWidth, anim.sheets[1]:getHeight(), anim.sheets[1]:getDimensions())
	end -- Splits the sheet into quads of 1 image each
	
	self.walkAnim.offset = vec2(70, 111)
end

function AnimationManager:getSheet_Walk(vec)
	local shortestDistance = 1/0 -- Set to infinity
	local shortestIndex = nil
	
	for i = 1, 8 do
		local distance = (vec - self.lookup[i]):len2()
		if distance < shortestDistance then
			shortestDistance = distance
			shortestIndex = i
		end
	end

	return self.walkAnim.sheets[shortestIndex]
end

function AnimationManager:getAnim_Walk(soldierID)
	local dirVector = vec2(math.cos(self.host.soldierDir[soldierID]), math.sin(self.host.soldierDir[soldierID]))
	
	local sheet = self:getSheet_Walk(dirVector)
	
	local animCycleSpeedModifier = 0.6
	
	local quadIndex = (math.round(self.animTimelines[soldierID] * animCycleSpeedModifier) % self.walkAnim.frameCount) + 1
	local quad = self.walkAnim.quads[quadIndex]

	return sheet, quad
end

function AnimationManager:initBlock()
	self.blockAnim = {
		sheets = {},
		frameCount = 0,
		imgWidth = 0,
		quads = {},
		offset = vec2(0, 0)
	}
	
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_Down_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_DownL_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_Left_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_UpL_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_Up_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_UpR_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_Right_strip5.png"))
	table.insert(self.blockAnim.sheets, love.graphics.newImage(self.sheetPath .. "_block/_DownR_strip5.png"))
	
	self.blockAnim.frameCount = 5
	self.blockAnim.imgWidth = self.blockAnim.sheets[1]:getWidth() / self.blockAnim.frameCount
	
	local anim = self.blockAnim
	for i = 1, anim.frameCount do
		local j = i - 1 -- To simplify things since lua arrays count from 1...
		-- We assume that all sheets are the same dimensions
		anim.quads[i] = love.graphics.newQuad(j * anim.imgWidth, 0, anim.imgWidth, anim.sheets[1]:getHeight(), anim.sheets[1]:getDimensions())
	end -- Splits the sheet into quads of 1 image each
	
	self.blockAnim.offset = vec2(112, 119)
end

function AnimationManager:getSheet_Block(vec)
	local shortestDistance = 1/0 -- Set to infinity
	local shortestIndex = nil
	
	for i = 1, 8 do
		local distance = (vec - self.lookup[i]):len2()
		if distance < shortestDistance then
			shortestDistance = distance
			shortestIndex = i
		end
	end

	return self.blockAnim.sheets[shortestIndex]
end

function AnimationManager:getAnim_Block(soldierID)
	local dirVector = vec2(math.cos(self.host.soldierDir[soldierID]), math.sin(self.host.soldierDir[soldierID]))
	
	local sheet = self:getSheet_Block(dirVector)
	
	local quadIndex = 4
	local quad = self.blockAnim.quads[quadIndex]

	return sheet, quad
end

function AnimationManager:getAnimFromID(soldierID)
	local sheet, quad
	local offset
	
	local cutoffVel = 3.0 -- Speed at which soldiers change from block to walk
	
	if self.host.soldierVel[soldierID]:len2() < cutoffVel*cutoffVel then
		-- Block animation
		sheet, quad = self:getAnim_Block(soldierID)
		offset = self.blockAnim.offset
	else
		-- Walk animation
		sheet, quad = self:getAnim_Walk(soldierID)
		offset = self.walkAnim.offset
	end

	
	return sheet, quad, offset
end

function AnimationManager:updateAnimStates(dt)
	local cutoffVel = 3.0 -- Speed at which soldiers restart walk cycle
	
	for soldierID = 1, self.host.soldierCount do
		if self.host.soldierVel[soldierID]:len2() < cutoffVel*cutoffVel then
			-- Block animation
			self.animTimelines[soldierID] = 0 -- Reset walk animation
		else
			-- Running animation
			self.animTimelines[soldierID] = self.animTimelines[soldierID] + (self.host.soldierVel[soldierID]:len() * dt)
		end
	end
end