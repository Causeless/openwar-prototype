function love.conf(t)
	t.window.width  = 1920
	t.window.height = 1080
	
	t.window.vsync = true
	
	t.window.fullscreen = true
    t.window.fullscreentype = "desktop" -- normal or desktop
	
	t.console = true
end