class = require "hump.class"
vec2 = require "hump.vector"
require "SteeringManager"
require "FormationManager"
require "AnimationManager"
require "Grid"
require "VectorTools"
require "SlotAssignmentStrategy"

Unit = class{}

function Unit:init(soldierCount, imagePath, r, g, b)
    -- Testing constants
    mass = 1
	
    -- Data-orientated design - soldiers only exist implicitly, with their data being linear in memory (cache friendly)
	-- DOD:(pos,pos,pos,vel,vel,vel,dir,dir,dir) instead of OOP:(pos,vel,dir,pos,vel,dir,pos,vel,dir)

	-- Also this isn't even cache-friendly, in fact it's less cache friendly than normal
	-- As pos, vel and dir are often used together but are more likely to be pushed out of cache line now
	
	-- DOD was a terrible, terrible idea without a clear interface. //TODO: FIX IT.
    
	-- Unit-wide stuff
    self.soldierCount = soldierCount
	self.soldierRadius = 5
    self.inverseMass = 1 / mass
    self.maxAccel = 100
	self.maxAccelSqr = self.maxAccel * self.maxAccel
	--self.maxDecel = 1200	-- TODO: Implement separate accel and decel forces? lerp between both depending on angle of accel vector compared to velocity vector? Perhaps friction will account for this anyways...
    self.runSpeed = 25
	self.walkSpeed = 15

	-- TODO: implement this
	-- Max degrees a soldier can turn per second
	self.maxTurnSpeed = 10;
	
	self.colourR = r or 0
	self.colourG = g or 0
	self.colourB = b or 0
	
    -- Soldier stuff
    self.soldierPos = {}    -- Positions
    self.soldierVel = {}    -- Velocities
    self.soldierAcc = {}    -- Accelerations
    
    self.soldierDir = {}    -- Directions
	
	self.soldierAnimFrames = {}
    
    for i = 1, self.soldierCount do
        self.soldierPos[i] = vec2(0, 0)
        self.soldierVel[i] = vec2(0, 0)
        self.soldierAcc[i] = vec2(0, 0)
        
        self.soldierDir[i] = 0
		self.soldierAnimFrames = 0
        --self.soldierTor[i] = 0
    end
	
	-- Various behavioural handlers
	self.grid = Grid(self.soldierPos)
    self.steering = SteeringManager(self)
	self.formation = FormationManager(self)
	self.animator = AnimationManager(self, imagePath)
end

function Unit:setPositions(targets)
	for i = 1, self.soldierCount do
        self.soldierPos[i] = targets[i].pos
        self.soldierVel[i] = vec2(0, 0)
        self.soldierAcc[i] = vec2(0, 0)
        
        self.soldierDir[i] = targets[i].dir
        --self.soldierTor[i] = 0
    end
end

function Unit:update(dt)
	self.grid:rebuild(self.soldierCount)
	
	self.formation:update(dt)
	self:performSteeringBehaviours(dt);
    
    for i = 1, self.soldierCount do
		self.soldierAcc[i] = truncate(self.soldierAcc[i], self.maxAccel)
	
		--if i == 1 then print(self.soldierVel[i]:len()) end
	
		self.soldierVel[i] = self.soldierVel[i] - (self.soldierVel[i] * 2 * dt)	-- Friction	
        self.soldierVel[i] = self.soldierVel[i] + (self.soldierAcc[i] * dt)
		self.soldierVel[i] = truncate(self.soldierVel[i], self.runSpeed)
		
        self.soldierPos[i] = self.soldierPos[i] + (self.soldierVel[i] * dt)
		
		local dirVec = vec2(math.cos(self.soldierDir[i]), math.sin(self.soldierDir[i]))
		
		local newDirVec = (dirVec) * 0.7 + (self.soldierVel[i] * dt) * 0.3 -- We don't normalize the vel as moving faster should incur faster rotation
		self.soldierDir[i] = math.atan2(newDirVec.y, newDirVec.x)
    end
end

function Unit:drawVelocity()
    for i = 1, self.soldierCount do
        love.graphics.line(self.soldierPos[i].x, self.soldierPos[i].y, self.soldierPos[i].x + self.soldierVel[i].x, self.soldierPos[i].y + self.soldierVel[i].y)
    end
end

function Unit:getTargetCanvas()
	if not targetCanvas then -- Lazily initialize
		local scaleFactor = 4
		invScaleFactor = 1/scaleFactor
		local radius = 5 * scaleFactor
		
		targetCanvas = love.graphics.newCanvas(radius*2 + 2, radius*2 + 2, "normal", 42)
		love.graphics.setCanvas(targetCanvas)
		love.graphics.push()
		
		local x, y = targetCanvas:getDimensions()
		love.graphics.translate(x*0.5, y*0.5)
		
		local halfpi = math.pi * 0.5
		
		local dir = 0
		local invDir = dir - math.pi
		
		local left = invDir - (halfpi*0.8)
		local right = invDir + (halfpi*0.8)
		
		local dirVec = vec2(math.cos(dir), math.sin(dir)) * radius
		local leftVec = vec2(math.cos(left), math.sin(left)) * radius
		local rightVec = vec2(math.cos(right), math.sin(right)) * radius
		
		love.graphics.arc("fill", 0, 0, radius, left, right, 30)
		love.graphics.polygon("fill", leftVec.x, leftVec.y, dirVec.x, dirVec.y, rightVec.x, rightVec.y)
		
		love.graphics.pop()
		
		love.graphics.setCanvas()
	end
	
	return targetCanvas, invScaleFactor
end

function Unit:drawTarget(pos, dir)
	--[[local radius = 4
	local halfpi = math.pi * 0.5
	
	local invDir = dir - math.pi
	
	local left = invDir - (halfpi*0.8)
	local right = invDir + (halfpi*0.8)
	
	local dirVec = vec2(math.cos(dir), math.sin(dir)) * radius
	local leftVec = vec2(math.cos(left), math.sin(left)) * radius
	local rightVec = vec2(math.cos(right), math.sin(right)) * radius
	
	love.graphics.arc("fill", pos.x, pos.y, radius, left, right, 10)
	love.graphics.polygon("fill", pos.x+leftVec.x, pos.y+leftVec.y, pos.x+dirVec.x, pos.y+dirVec.y, pos.x+rightVec.x, pos.y+rightVec.y)]]
	
	local canvas, scaleFactor = Unit:getTargetCanvas()
	local centreX, centreY = canvas:getWidth()/2, canvas:getHeight()/2
	love.graphics.draw(canvas, pos.x, pos.y, dir, scaleFactor, scaleFactor, centreX, centreY)
end

function Unit:drawTargets()
	local targets = self.formation:getTargetsFinalLocation()
	
	for i = 1, #targets do
		local pos = targets[i].pos
		local dir = targets[i].dir
		
		Unit:drawTarget(pos, dir)
	end
end

function Unit:drawSteering()
    for i = 1, self.soldierCount do
        love.graphics.line(self.soldierPos[i].x, self.soldierPos[i].y, self.soldierPos[i].x + self.soldierAcc[i].x, self.soldierPos[i].y + self.soldierAcc[i].y)
    end
end

function Unit:applyForce(soldierID, force)
    self.soldierAcc[soldierID] = force * self.inverseMass
end

function Unit:addForce(soldierID, force)
    self.soldierAcc[soldierID] = self.soldierAcc[soldierID] + (force * self.inverseMass)
end

function Unit:applyAllForces(forces)
    for i = 1, self.soldierCount do
        self.soldierAcc[i] = forces[i] * self.inverseMass
    end
end

function Unit:addAllForces(forces)
    for i = 1, self.soldierCount do
        self.soldierAcc[i] = self.soldierAcc[i] + (forces[i] * self.inverseMass)
    end
end

function Unit:clearForces()
	for i = 1, self.soldierCount do
        self.soldierAcc[i] = vec2(0, 0)
    end
end

function Unit:setSoldierTargetPairs(soldierTargetPairs)
	self.soldierTargetPairs = soldierTargetPairs
end

function Unit:performSteeringBehaviours(dt)
	if self.steering ~= nil then
		self.steering.checks = {}
		self:clearForces()
		--self.steering:align()
		self.steering:divert()
		self.steering:avoid()
		self.steering:separate()
		if #self.formation.targets > 0 then
			local targets = self.formation:getTargetsWorldSpace()
			--local targets = self.formation:getTargetsFinalLocation()
			--self.soldierTargetPairs = SlotAssignmentStrategy:assignTargetsCentreOut(self, targets)
			self.steering:seekPoints(targets, self.soldierTargetPairs)
		end
	end
end

function Unit:findAveragePosition()
	vecAddition = vec2(0, 0)
	for i = 1, self.soldierCount do
		vecAddition = vecAddition + self.soldierPos[i]
	end
	return vecAddition / self.soldierCount
end

function Unit:removeSoldier(soldierID)
	table.remove(self.soldierPos, soldierID)
	table.remove(self.soldierVel, soldierID)
	table.remove(self.soldierAcc, soldierID)
	table.remove(self.soldierDir, soldierID)
    
    self.soldierCount = self.soldierCount - 1
end

function Unit:swapSoldiers(soldierID1, soldierID2)
	local tempPos = self.soldierPos[soldierID1]
	local tempVel = self.soldierVel[soldierID1]
	local tempAcc = self.soldierAcc[soldierID1]
	local tempDir = self.soldierDir[soldierID1]
	
	self.soldierPos[soldierID1] = self.soldierPos[soldierID2]
	self.soldierVel[soldierID1] = self.soldierVel[soldierID2]
	self.soldierAcc[soldierID1] = self.soldierAcc[soldierID2]
	self.soldierDir[soldierID1] = self.soldierDir[soldierID2]
	
	self.soldierPos[soldierID2] = tempPos
	self.soldierVel[soldierID2] = tempVel
	self.soldierAcc[soldierID2] = tempAcc
	self.soldierDir[soldierID2] = tempDir
end